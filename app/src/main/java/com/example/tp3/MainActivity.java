package com.example.tp3;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView listTeam;
    public static SwipeRefreshLayout ly;
    SportDbHelper db;
    public static TeamRecyclerAdapter adapter;

    public static SportDbHelper DB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        MainActivity.DB = new SportDbHelper(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                MainActivity.this.startActivityForResult(myIntent,1);
            }
        });

        db = new SportDbHelper(this);

        this.linkFields();
        this.initFields();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {

            Team team = (Team) data.getParcelableExtra(Team.TAG);

            if (team != null) {
                MainActivity.DB.addTeam(team);
                Toast.makeText(this, " saved !", Toast.LENGTH_SHORT);
            } else {
                Toast.makeText(this, "erreur !", Toast.LENGTH_SHORT);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        for (Team team : MainActivity.DB.getAllTeams()) {
            RefreshTeamContent runningTask = new RefreshTeamContent();
            runningTask.execute(team);
        }
    }
    private void linkFields() {
        this.listTeam = findViewById(R.id.listTeam);
        ly = findViewById(R.id.ly);
    }

    private void initFields() {

        this.initList();
        ly.setOnRefreshListener(this);
        new ItemTouchHelper(removeCallback).attachToRecyclerView(listTeam);
    }
    private void initList() {
        adapter = new TeamRecyclerAdapter(this);
        this.listTeam.setAdapter(adapter);
        listTeam.setLayoutManager(new LinearLayoutManager(this));

    }
    ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int position = viewHolder.getAdapterPosition();
            long id = MainActivity.DB.getAllTeams().get(position).getId();

            MainActivity.DB.deleteTeam(id);

            adapter.notifyDataSetChanged();
        }
    };

    private final class RefreshTeamContent extends AsyncTask<Team, Void, String> {

        @Override
        protected String doInBackground(Team... params) {

            Team team = params[0];
            JSONResponseHandlerTeam responseHandlerTeam = new JSONResponseHandlerTeam(team);
            TeamActivity.loadTeamLastEvent(team, responseHandlerTeam, MainActivity.this);
            TeamActivity.loadTeamRank(team, responseHandlerTeam, MainActivity.this);
            MainActivity.DB.updateTeam(team);
            return TeamActivity.DONE;
        }

        @Override
        protected void onPostExecute(String result) {

                MainActivity.adapter.notifyDataSetChanged();
                MainActivity.ly.setRefreshing(false);
                Log.d("RefreshTeamContent","Finished");
            }
        }
    }

