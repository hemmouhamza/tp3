package com.example.tp3;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import com.example.tp3.Team;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    /**
     * The team
     */
    private Team team;

    /**
     * Constructor
     * @param team
     */
    public JSONResponseHandlerTeam(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Parse to JSON the response of the last event API
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStreamLastEvent(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readLastEvent(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Parse to JSON the response of the last event API
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStreamRank(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readRank(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Read the events
     * @param reader
     * @throws IOException
     */
    public void readLastEvent(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("results")) {
                readArrayLastEvent(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    /**
     * Read the ranks
     * @param reader
     * @throws IOException
     */
    public void readRank(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("table")) {
                readArrayRank(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    /**
     * Read the teams
     * @param reader
     * @throws IOException
     */
    public void readTeams(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("teams")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    /**
     * Read the first occurence of the team
     * @param reader
     * @throws IOException
     */
    private void readArrayTeams(JsonReader reader) throws IOException {

        reader.beginArray();

        int nb = 0; // only consider the first element of the array

        while (reader.hasNext() ) {

            reader.beginObject();

            while (reader.hasNext()) {

                String name = reader.nextName();

                if (nb==0) {

                    if (name.equals("idTeam")) {
                        team.setIdTeam(reader.nextLong());
                    } else if (name.equals("strTeam")) {
                        team.setName(reader.nextString());
                    } else if (name.equals("strLeague")) {
                        team.setLeague(reader.nextString());
                    } else if (name.equals("idLeague")) {
                        team.setIdLeague(reader.nextLong());
                    } else if (name.equals("strStadium")) {
                        team.setStadium(reader.nextString());
                    } else if (name.equals("strStadiumLocation")) {
                        team.setStadiumLocation(reader.nextString());
                    } else if (name.equals("strTeamBadge")) {
                        team.setTeamBadge(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }

            reader.endObject();

            nb++;
        }

        reader.endArray();
    }

    /**
     * Read the first last event
     * @param reader
     * @throws IOException
     */
    private void readArrayLastEvent(JsonReader reader) throws IOException {

        /**
         * The output match
         */
        Match match = new Match();

        // Start to read the array
        reader.beginArray();

        int nb = 0; // only consider the first element of the array

        // For each last event
        while (reader.hasNext() ) {

            reader.beginObject();

            // For each data
            while (reader.hasNext()) {

                // Data label
                String name = reader.nextName();

                // Only for the firs t match
                if (nb==0) {

                    // Get the informations below
                    if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        match.setHomeScore(reader.nextInt());
                    } else if (name.equals("intAwayScore")) {
                        match.setAwayScore(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }

                }  else {
                    reader.skipValue();
                }
            }

            reader.endObject();

            nb++;
        }

        reader.endArray();

        // Set the match informations
        team.setLastEvent(match);
    }

    /**
     * Read the rank
     * @param reader
     * @throws IOException
     */
    private void readArrayRank(JsonReader reader) throws IOException {

        reader.beginArray();

        int index = 0; // current index

        // For each team
        while (reader.hasNext() ) {

            reader.beginObject();

            ++index;

            // For each fields
            while (reader.hasNext()) {

                // Get the field name
                String name = reader.nextName();

                // If the field is the name of the team
                if (name != null && name.equals("name")) {

                    // The api current team
                    String teamName = reader.nextString();
                    // The current tea
                    String currentTeamName = team.getName();

                    // If the name of the team is the same as we want
                    if (teamName != null && teamName.equals(currentTeamName)) {

                        // Set up the index
                        team.setRanking(index);
                    }

                    while (reader.hasNext()) {

                        // Get the field name
                        String nameBis = reader.nextName();

                        if (nameBis.equals("total")) {
                            team.setTotalPoints(reader.nextInt());
                            break;
                        } else {
                            reader.skipValue();
                        }
                    }

                } else {

                    if (reader.hasNext()) {
                        reader.skipValue();
                    }
                }
            }

            reader.endObject();
        }

        reader.endArray();
    }

}
